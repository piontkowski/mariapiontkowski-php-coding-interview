<?php

namespace Src\models;

use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;
	private $helper;
	private $dog;

	function __construct() {
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		
		$this->bookingData = json_decode($string, true);
		$this->helper = new Helpers();
		$this->dog = new DogModel();
	}

	public function getBookings() {
		return $this->bookingData;
	}
	
	public function createBooking($data) {
		
		$bookings = $this->getBookings();
		
		$clientDogsAge = $this->dog->getDogsAgeByClient($data['clientid']);
		
		$price = $data['price'];
		
		if(count($clientDogsAge) && array_sum($clientDogsAge) / count($clientDogsAge) < 10)
			$price -= $price * 0.1;
		
		$data["id"] = end($bookings)['id'] + 1;
		$data["price"] = $price;
		
		$bookings[] = $data;
		
		$this->helper->putJson($bookings, 'bookings');
		
		return $data;
		
	}
}