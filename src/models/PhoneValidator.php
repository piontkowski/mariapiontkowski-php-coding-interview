<?php
	
	namespace Src\models;
	
	class PhoneValidator {
		
		//Ex 4158586273
		protected $phone;
		
		//Ex US
		protected $countyCode;
		
		function __construct($phone, $countyCode = 'US')
		{
			$this->phone = $phone;
			$this->countyCode = $countyCode;
		}
		
		public function isValid() {
			$response = file_get_contents("http://apilayer.net/api/validate?access_key=e3054d5288af540d892db78be86d0dbf&number={$this->phone}&country_code={$this->countyCode}&format=1");
			
			return json_decode($response)->valid;
		}
	}
	
	