<?php
	
	
	namespace Tests;
	
	use PHPUnit\Framework\TestCase;
	use Src\models\PhoneValidator;
	
	class PhoneValidatorTest extends TestCase
	{
		public function testValidPhone() {
			$phoneValidator = new PhoneValidator('4158586273');
			
			$this->assertTrue($phoneValidator->isValid());
		}
	}